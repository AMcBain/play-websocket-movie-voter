import sys
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.parse import urlencode
import re
import json
import time

def main(args=None):
    """Fetch movies from IMDB based on a file with a newline separated list of titles in it."""
    
    if args is None or len(args) < 2:
        print("Usage: py movies.py filename.txt\n")
        print("Movie titles should be separated by newlines. Partial titles are acceptable but")
        print("try to be specific. If two films with the same name need to be differentiated")
        print("you can append a year to the end as follows:\n")
        print("    Alice in Wonderland (2010)\n")
        print("Data provided by http://www.omdbapi.com/")
        return
    
    names = open(args[1], "r").read().replace("\r\n", "\n").replace("\r", "\n").split("\n")
    
    categories = set()
    movies = list()
    
    with open("conf/data.yml", "w") as output:
        
        for name in names:
            if len(name) > 0:
                year = re.search(r"\(\d+\)$", name)
                
                if year is None:
                    year = "";
                else:
                    year = year.group(0)[1:-1]
                    name = re.sub(r"\s+\(\d+\)$", "", name)
                
                try:
                    print(name, end="");
                    req = urlopen("http://www.omdbapi.com/?{}".format(urlencode([("t", name), ("y", year), ("type", "movie"), ("r", "json")])));
                    data = json.loads(req.read().decode(req.headers.get_content_charset()))
                    print(" -> {} ({})".format(data["Title"], data["Year"]))
                    
                    genres = data["Genre"].split(", ")
                    
                    for genre in genres:
                        if genre not in categories:
                            categories.add(genre)
                            output.write("Category({}):\n".format(genre))
                            output.write("    name: {}\n\n".format(genre))
                    
                    movies.append(data)
                except HTTPError as err:
                    print(" -> {}: {}".format(err.code, err.reason))
                
                # Friendliness. Works out to ~180 requests in 15 minutes.
                time.sleep(5)
        
        check = dict()
        id = 1
        
        for movie in movies:
            if movie["Title"] not in check or check[movie["Title"]]["Year"] != movie["Year"]:
                
                check[movie["Title"]] = movie
                year = ""
                
                if check[movie["Title"]]["Year"] != movie["Year"]:
                    year = " ({})".format(movie["Year"])
                
                output.write("\nMovie(m{}):\n".format(id))
                output.write("    title:      \"{}{}\"\n".format(movie["Title"], year))
                output.write("    categories: [{}]\n".format(movie["Genre"]))
                # For internal purposes, indicates if a film in your collection is on disk or digital. It's not currently
                # used but it could be displayed as an asterisk or dagger to discourage picking it if one or the other
                # type is of an inferior quality.
                output.write("    ripped:     false\n")
                output.write("    length:     {}\n".format(movie["Runtime"].split(" ")[0]))
                
                id += 1

if __name__ == "__main__":
    main(sys.argv)
