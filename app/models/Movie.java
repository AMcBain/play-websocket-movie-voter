package models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import play.db.jpa.Model;

@Entity
public class Movie extends Model
{
    public String title;

    @ManyToMany(fetch=FetchType.EAGER)
    public List<Category> categories;

    public boolean ripped;

    public int length;

    public Movie ()
    {
    }

    public String getRunTime ()
    {
        int hours = length / 60;
        int minutes = length % 60;

        return hours + "h " + (minutes > 0 ? minutes + "min" : "");
    }

    /**
     * Used for the serialization of a top-level set of Categories to skip serializing further than each Movie.
     */
    public static class Serializer implements JsonSerializer<Movie>
    {
        @Override
        public JsonElement serialize(final Movie movie, final Type type, final JsonSerializationContext context)
        {
            JsonObject object = new JsonObject();
            object.addProperty("title", movie.title);

            JsonArray categories = new JsonArray();

            for (Category category : movie.categories)
            {
                categories.add(category.name);
            }

            object.add("categories", categories);
            object.addProperty("ripped", movie.ripped);
            object.addProperty("length", movie.length);
            object.addProperty("id", movie.id);

            return object;
        }
    }
}
