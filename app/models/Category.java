package models;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import play.db.jpa.Model;

@Entity
public class Category extends Model
{
    public String name;

    @ManyToMany(mappedBy="categories")
    public List<Movie> movies;

    public Category ()
    {
    }

    public Category (Category category)
    {
        name = category.name;
        movies = new ArrayList<Movie>();
    }

    public int hashCode ()
    {
        return name.hashCode();
    }

    public String toString ()
    {
        return name;
    }

    /**
     * Used for the serialization of a top-level list of Movies to skip serializing further than each Category.
     */
    public static class Serializer implements JsonSerializer<Category>
    {
        @Override
        public JsonElement serialize(final Category category, final Type type, final JsonSerializationContext context)
        {
            return new JsonPrimitive(category.name);
        }
    }
}
