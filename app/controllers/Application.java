package controllers;

import java.util.List;

import models.Movie;

import play.mvc.Controller;

public class Application extends Controller
{
    public static void index ()
    {
        List<Movie> movies = Movie.find("order by title").fetch();
        render(movies);
    }

    public static void graph ()
    {
        render();
    }

    public static void clear ()
    {
        WebSocket.clear();
        index();
    }
}
