package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import models.Category;
import models.Movie;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import play.libs.F.Matcher;
import play.mvc.Http;
import play.mvc.Http.WebSocketEvent;
import play.mvc.WebSocketController;

public class WebSocket extends WebSocketController
{
    private static ConcurrentMap<String, Http.Outbound> GRAPHS = new ConcurrentHashMap<>();
    private static ConcurrentMap<String, Http.Outbound> VOTERS = new ConcurrentHashMap<>();
    private static ConcurrentMap<String, Movie> VOTES = new ConcurrentHashMap<>();

    private static Comparator<Category> ASC = new Comparator<Category> ()
    {
        public int compare(Category a, Category b)
        {
            return a.name.compareTo(b.name);
        }
    };

    private static Comparator<Category> DESC = new Comparator<Category> ()
    {
        public int compare(Category a, Category b)
        {
            return b.name.compareTo(a.name);
        }
    };

    public static boolean clear ()
    {
        VOTES.clear();
        notifyGraphs();

        for (Http.Outbound voter : VOTERS.values())
        {
            voter.send("unvote");
        }

        return true;
    }

    public static void listen (String id)
    {
        GRAPHS.put(id, outbound);
        notifyGraphs();

        while (inbound.isOpen())
        {
            WebSocketEvent event = await(inbound.nextEvent());

            for (String message : WebSocketEvent.TextFrame.match(event))
            {
                if ("ping".equals(message))
                {
                    outbound.send("pong");
                }
            }
        }

        GRAPHS.remove(id);
    }

    private static boolean notifyGraphs ()
    {
        Map<Long, Tally> tallies = new HashMap<>();

        for (Movie movie : VOTES.values())
        {
            if (!tallies.containsKey(movie.id))
            {
                tallies.put(movie.id, new Tally(movie));
            }
            tallies.get(movie.id).count += 1;
        }

        String data = new GsonBuilder()
                .registerTypeAdapter(Category.class, new Category.Serializer())
                .create()
                .toJson(tallies);

        for (Http.Outbound socket : GRAPHS.values())
        {
            socket.send(data);
        }

        // Just in case, so Play doesn't treat it as a routable method as it does for regular controllers.
        return true;
    }

    public static void talk (String id)
    {
        VOTERS.put(id, outbound);

        while (inbound.isOpen())
        {
            WebSocketEvent event = await(inbound.nextEvent());

            for (String message : WebSocketEvent.TextFrame.match(event))
            {
                if ("ping".equals(message))
                {
                    outbound.send("pong");
                    continue;
                }
                // Why? Well, because the Application controller can't see the VOTES map.
                //      Even if it could, it doesn't know our ID.
                // Why? Because storing the ID requires a cookie. It's a bit of catering
                //      to some "security conscious" people I know.
                else if ("vote?".equals(message))
                {
                    if (VOTES.containsKey(id))
                    {
                        JsonObject object = new JsonObject();
                        object.addProperty("vote", VOTES.get(id).id);

                        outbound.send(new Gson().toJson(object));
                    }
                    continue;
                }

                try
                {
                    // Ugh. So verbose and crap. Should have just brought in another library.
                    JsonElement element = new JsonParser().parse(message);

                    if (element.isJsonObject())
                    {
                        JsonObject object = element.getAsJsonObject();

                        if (object.get("vote") != null)
                        {
                            Movie movie = Movie.find("byId", object.get("vote").getAsLong()).first();

                            if (movie != null)
                            {
                                VOTES.put(id, movie);
                            }
                            notifyGraphs();

                            // Nobody said I was winning any prizes for code quality here.
                            continue;
                        }

                        boolean desc = object.get("desc") != null ? object.get("desc").getAsBoolean() : true;
                        String sortBy = object.get("sortBy") != null ? object.get("sortBy").getAsString() : "alphabetical";

                        String orderBy = "order by ";
                        List<Movie> movies;

                        if ("alphabetical".equals(sortBy) || "category".equals(sortBy))
                        {
                            orderBy += "title" + (desc ? " desc" : "") + ", length";
                        }
                        else
                        {
                            orderBy += "length" + (desc ? " desc" : "") + ", title";
                        }

                        JsonElement text = object.get("filter");
                        if (text != null && !text.getAsString().trim().isEmpty())
                        {
                            movies = Movie.find("lower(title) like ?1 " + orderBy, "%" + text.getAsString().trim().toLowerCase() + "%").fetch();
                        }
                        else
                        {
                            movies = Movie.find(orderBy).fetch();
                        }

                        if ("category".equals(sortBy))
                        {
                            // Would use a SortedSet except I can't get values back out.
                            List<Category> categories = new ArrayList<>();
                            Map<Category, Category> stash = new HashMap<>();

                            for (Movie movie : movies)
                            {
                                for (Category category : movie.categories)
                                {
                                    if (!stash.containsKey(category))
                                    {
                                        stash.put(category, new Category(category));
                                        categories.add(stash.get(category));
                                    }
                                    stash.get(category).movies.add(movie);
                                }
                            }

                            Collections.sort(categories, desc ? DESC : ASC);

                            Gson gson = new GsonBuilder()
                                    .registerTypeAdapter(Movie.class, new Movie.Serializer())
                                    .create();
                            outbound.send(gson.toJson(categories));
                        }
                        else
                        {
                            Gson gson = new GsonBuilder()
                                    .registerTypeAdapter(Category.class, new Category.Serializer())
                                    .create();
                            outbound.send(gson.toJson(movies));
                        }
                    }
                }
                catch (JsonParseException|ClassCastException e)
                {
                    // Don't care, really. If it doesn't pass, it wasn't sent by us.
                }
            }
        }

        VOTERS.remove(id);
    }

    /**
     * It's this or store a count value on each movie that needs reset before each send-off.
     */
    private static class Tally
    {
        public Movie movie;
        public int count;

        public Tally (Movie movie)
        {
            this.movie = movie;
        }
    }
}
