import models.Movie;

import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;
 
@OnApplicationStart
public class Bootstrap extends Job
{
    public void doJob()
    {
        if (Movie.count() == 0)
        {
            Fixtures.load("data.yml");
        }
    }
}
