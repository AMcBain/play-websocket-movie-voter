"use strict";

// If I was trying to win an award for pretty code, this wouldn't be it.
document.addEventListener("DOMContentLoaded", function ()
{
    var websocket, timeout, filter = document.querySelector("input"), sortBy = document.querySelector("select"),
            asc = document.querySelector(".sort-asc"), desc = document.querySelector(".sort-desc"),
            list = document.querySelector("ol"), graph = document.querySelector("ul"), voted;

    function sort ()
    {
        if (asc.classList.contains("active"))
        {
            asc.classList.remove("active");
            desc.classList.add("active");
        }
        else
        {
            desc.classList.remove("active");
            asc.classList.add("active");
        }

        send({
            filter: filter.value,
            sortBy: sortBy.value,
            desc: desc.classList.contains("active")
        });
    }

    if (asc && desc)
    {
        asc.addEventListener("click", sort);
        desc.addEventListener("click", sort);
    }

    if (sortBy)
    {
        sortBy.value = "alphabetical";
        sortBy.addEventListener("change", function ()
        {
            send({
                filter: filter.value,
                sortBy: sortBy.value,
                desc: desc.classList.contains("active")
            });
        });
    }

    if (filter)
    {
        filter.value = "";
        filter.addEventListener("input", function (event)
        {
            clearTimeout(timeout);

            timeout = setTimeout(function ()
            {
                send({
                    filter: filter.value,
                    sortBy: sortBy.value,
                    desc: desc.classList.contains("active")
                });
            }, 200);
        });
    }

    if (list)
    {
        list.addEventListener("click", function (event)
        {
            var selected, target = event.target;

            while (target && target.dataset && !target.dataset.id)
            {
                target = target.parentNode;
            }

            if (target && target.dataset)
            {
                selected = list.querySelectorAll(".selected");

                if (selected.length)
                {
                    Array.prototype.forEach.call(selected, function (li)
                    {
                        li.classList.remove("selected");
                    });
                }

                Array.prototype.forEach.call(list.querySelectorAll("[data-id='" + target.dataset.id + "']"), function (li)
                {
                    li.classList.add("selected");
                });

                send({
                    vote: parseInt(target.dataset.id)
                });
                voted = parseInt(target.dataset.id);
            }
        });
    }

    // Great. Duplicated code. :|
    function getRunTime (length)
    {
        var hours = Math.floor(length / 60);
        var minutes = length % 60;

        return hours + "h " + (minutes > 0 ? minutes + "min" : "");
    }

    function send (message)
    {
        initWS(function ()
        {
            websocket.send(JSON.stringify(message));
        });
    }

    function movie (movie)
    {
        var h3 = document.createElement("h3");
        h3.textContent = movie.title;

        var span1 = document.createElement("span");
        span1.textContent = movie.categories.join(", ");

        var span2 = document.createElement("span");
        span2.textContent = getRunTime(movie.length);

        var li = document.createElement("li");
        li.tabIndex = 0;
        li.dataset.id = movie.id;
        if (movie.id === voted)
        {
            li.classList.add("selected");
        }
        li.appendChild(h3);
        li.appendChild(span1);
        li.appendChild(document.createTextNode(" "));
        li.appendChild(span2);

        list.appendChild(li);
    }

    function remove (element)
    {
        element.addEventListener("transitionend", function ()
        {
            // Apparently transitionend fires for each property that is transitioning when it finishes.
            // This makes sense as you can transition each property differently, but it is annoying.
            if (element.parentNode)
            {
                element.parentNode.removeChild(element);
            }
        });

        element.style.width = 0;
    }

    function initWS (callback)
    {
        var open, key;

        if (!websocket || websocket.readyState === WebSocket.CLOSING || websocket.readyState === WebSocket.CLOSED)
        {
            // GUID generation - http://stackoverflow.com/a/2117523/251262
            key = window.localStorage.keyid || "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c)
            {
                var r = Math.random() * 16 | 0;
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            window.localStorage.keyid = key;

            websocket = new WebSocket((location.protocol === "https:" ? "wss" : "ws") + "://" + location.host + (list ? "/talk" : "/listen") + "/" + key);
            websocket.addEventListener("message", function (event)
            {
                var data, item, li, span, max = 0;

                if (event.data === "pong")
                {
                    return;
                }
                else if (event.data === "unvote")
                {
                    if (list)
                    {
                        data = list.querySelector(".selected");

                        if (data)
                        {
                            data.classList.remove("selected");
                        }
                        voted = null;
                    }
                    else
                    {
                        graph.innerHTML = "";
                    }
                    return;
                }

                try
                {
                    data = JSON.parse(event.data);

                    if (data.vote)
                    {
                        li = document.querySelector("[data-id='" + data.vote + "']");

                        if (li)
                        {
                            li.classList.add("selected");
                        }
                        return;
                    }

                    if (list)
                    {
                        list.innerHTML = "";

                        if (data[0] && data[0].title)
                        {
                            data.forEach(movie);
                        }
                        else
                        {
                            data.forEach(function (category)
                            {
                                var h2 = document.createElement("h2");
                                h2.textContent = category.name;

                                var li = document.createElement("li");
                                li.appendChild(h2);
                                list.appendChild(li);

                                category.movies.forEach(movie);
                            });
                        }
                    }
                    else
                    {
                        for (item in data)
                        {
                            max = Math.max(max, data[item].count);
                        }

                        Array.prototype.forEach.call(graph.children, function (li)
                        {
                            if (!data[li.dataset.id] || !data[li.dataset.id].count)
                            {
                                li.children[li.children.length - 1].textContent = "";
                                remove(li);
                            }
                            else
                            {
                                li.children[li.children.length - 1].textContent = data[li.dataset.id].count;
                                li.style.width = data[li.dataset.id].count / max * 100 + "%";
                            }

                             delete data[li.dataset.id];
                        });

                        for (item in data)
                        {
                            li = document.createElement("li");
                            li.dataset.id = data[item].movie.id;
                            li.style.width = "0%";

                            // Give them time to sit on the DOM before we transition, otherwise it'll change instantly.
                            setTimeout(function ()
                            {
                                this.style.width = data[this.dataset.id].count / max * 100 + "%";
                            }.bind(li), 100);

                            span = document.createElement("span");
                            span.textContent = data[item].movie.title;
                            li.appendChild(span);
                            li.appendChild(document.createTextNode(" "));

                            span = document.createElement("span");
                            span.textContent = data[item].count;
                            li.appendChild(span);

                            graph.appendChild(li);
                        }
                    }
                }
                catch (e)
                {
                    // Well... sucks.
                    console.log(e);
                }
            });

            open = function ()
            {
                websocket.removeEventListener("open", open);
                callback();
            };
            websocket.addEventListener("open", open);
        }
        else
        {
            callback();
        }
    }

    initWS(function ()
    {
        if (list)
        {
            websocket.send("vote?");
        }
    });

    setTimeout(function ()
    {
        if (websocket && websocket.readyState === WebSocket.OPEN)
        {
            websocket.send("ping");
        }
    }, 501000);
});
